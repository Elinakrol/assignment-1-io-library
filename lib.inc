%define s_nine "9"
%define s_zero "0"
section .text
 
; Принимает код возврата и завершает текущий процесс

exit: 
    mov rax, 60
    syscall
    ret


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
      cmp byte [rdi+rax], 0 ; проверка на окончанияб строки код символа 0
      je .end               ; if символы закончились => переходит к .end
      inc rax               ; else переход к следующей итерации
      jmp .loop
    .end:
       ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov rsi, rdi
	push rsi
	call string_length
	mov rdx, rax
	pop rsi
	mov rax, 1
	mov rdi, 1
	syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
   	xor rax, rax
   	push rdi
    	mov rax, 1          ; системный код 1  
    	mov rdi, 1          ; stdout descriptor 1   
    	mov rsi, rsp        
    	mov rdx, 1          ; размер символа в байтах (код=1)
    	syscall
    	pop rdi
    	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, `\n`
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, `\n`
    mov r8, rsp
    dec rsp
    mov byte [rsp], 0
	.loop:
		xor rdx, rdx
		div rsi
		mov rdi, s_zero
		add rdi, rdx
		dec rsp
		mov byte [rsp], dil
		cmp rax, 0
		jne .loop
	mov rdi, rsp
	push r8
	call print_string
	pop r8
	mov rsp, r8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov rdi, 45
	call print_char
	pop rdi
	neg rdi
	jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	mov rdx, -1
      .loop:
	inc rdx
	mov al, byte [rdi+rdx]
	cmp al, byte [rsi+rdx]
	jne .cmp_equall
	cmp al, 0
	jne .loop
	mov rax, 1
	ret
      .cmp_equall:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	.for_space:
		push rdi
		push rsi
		call read_char
		pop rsi
		pop rdi
		cmp rax, 32
		je .for_space
		cmp rax,  `\t`
		je .for_space
		cmp rax, `\n`
		je .for_space
	xor rdx, rdx
	.loop:
		cmp rax, 32 
		je .end
		cmp rax, `\t`
		je .end
		cmp rax, 4
		je .end
		cmp rax, `\n`
		je .end
		cmp rax, 0
		je .end
		mov byte [rdi+rdx], al
		inc rdx
		cmp rdx, rsi
		jae .fail
		push rdi
		push rsi
		push rdx
		call read_char
		pop rdx
		pop rsi
		pop rdi
		jmp .loop
	.end:
		cmp rdx, rsi
		jae .fail
		mov byte [rdi+rdx],0
		mov rax, rdi
		ret
	.fail:
		xor rax, rax
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        xor rsi, rsi
        xor r9, r9
        mov r8, `\n`
.loop:
        mov sil, [rdi+r9]
        cmp sil, 0
        je .end
        cmp sil, s_zero
        jb .end
        cmp sil, s_nine
        ja .end
        mul r8
        sub sil, s_zero
        add rax, rsi
        inc r9
        jmp .loop
.end:
        mov rdx, r9
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'           
    je .negative_number         
    jmp parse_uint              
    .negative_number:
        inc rdi                 
        call parse_uint        
        inc rdx                 
        neg rax                 
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
	push rdi
	push rsi
	push rdx	
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jae .err
	xor rcx, rcx
	.loop:
	   mov al, byte [rdi+rcx]
	   cmp al, 0
       je .end
	   mov byte[rsi+rcx], al
       inc rcx
	   jmp .loop
	.end:
		mov byte[rsi+rcx], 0
		inc rcx
		mov rax, rcx
		ret
	.err:
		xor rax, rax
		ret
